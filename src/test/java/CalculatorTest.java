import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class CalculatorTest
{
    @Test
    public void addTest()
    {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(9, calculator.add(4,5));
    }
}
